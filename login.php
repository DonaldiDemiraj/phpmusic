<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
  <!-- Google Font: Source Sans Pro -->
  <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback"> -->
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="assets/plugins/fontawesome-free/css/all.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- DataTables -->
  <link rel="stylesheet" href="assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
   <!-- Select2 -->
  <link rel="stylesheet" href="assets/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
   <!-- SweetAlert2 -->
  <link rel="stylesheet" href="assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="assets/plugins/toastr/toastr.min.css">
  <!-- dropzonejs -->
  <link rel="stylesheet" href="assets/plugins/dropzone/min/dropzone.min.css">
  <!-- DateTimePicker -->
  <link rel="stylesheet" href="assets/dist/css/jquery.datetimepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Switch Toggle -->
  <!-- Ekko Lightbox -->
  <link rel="stylesheet" href="assets/plugins/ekko-lightbox/ekko-lightbox.css">
  <link rel="stylesheet" href="assets/plugins/bootstrap4-toggle/css/bootstrap4-toggle.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="assets/dist/css/images-grid.css">
  <link rel="stylesheet" href="assets/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="assets/dist/css/styles.css">
  <script src="assets/plugins/jquery/jquery.min.js"></script>

	<script src="assets/dist/js/images-grid.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="assets/plugins/jquery-ui/jquery-ui.min.js"></script>
 <!-- summernote -->
  <link rel="stylesheet" href="assets/plugins/summernote/summernote-bs4.min.css">

<!-- Ekko Lightbox -->
<script src="assets/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
  <title>home</title>
</head>
<body>
  <?php
    include('index.html');
  ?>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  

<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-primary navbar-dark bg-dark border-primary">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      
      <li  class="nav-link">
       <a ></a>
      </li>
      <li>
        <a class="nav-link text-gradient-primary"  href="./" role="button"> <large><b>Simple Music Clound Community</b></large></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->





  <aside class="main-sidebar sidebar-dark-navy bg-black elevation-4">
    <div class="dropdown">
   	<a href="javascript:void(0)" class="brand-link bg-black" data-toggle="dropdown" aria-expanded="true">
        <span class="brand-image img-circle elevation-3 d-flex justify-content-center align-items-center text-white font-weight-500" style="width: 38px;height:50px;font-size: 2rem"><b><i class="fa fa-headphones-alt text-gradient-primary"></i></b></span>
        <span class="brand-text font-weight-light  text-gradient-primary"><i>Music</i></span>

      </a>
    </div>
    <div class="sidebar">
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column nav-flat" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
          <a href="./index.php?page=homepage_music_list" class=" ml-1 nav-link nav-music_list tree-item">
              <i class="nav-icon fa fa-music text-gradient-primary"></i>
              <p>
              Musics List
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
          </li>
          <li class="nav-item">
                <a href="./index.php?page=blog" class="nav-link nav-blog tree-item">
                  <i class="fas fa-th-list nav-icon  text-gradient-primary"></i>
                  <p>Blog</p>
                </a>
          </li>  
          <li class="nav-item">
                <a href="./index.php?page=about" class="nav-link nav-about tree-item">
                  <i class="fas fa-th-list nav-icon  text-gradient-primary"></i>
                  <p>AboutUs</p>
                </a>
          </li>  
         
        </ul>
        <ul class="nav nav-pills nav-sidebar flex-column nav-flat">
        <li class="nav-item">
                <a href="./index.php?page=homepage" class="nav-link nav-genre_list tree-item">
                  <i class="fas fa-th-list nav-icon  text-gradient-primary"></i>
                  <p>Login/Register</p>
                </a>
          </li>  
          <li class="nav-item">
            <a href="./index.php?page=loginadmin" class="nav-link nav-genre_list tree-item">
              <i class="fas fa-th-list nav-icon  text-gradient-primary"></i>
              <p>Admin</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="./index.php?page=vote" class="nav-link nav-gener_list tree-item">
              <i class="fas fa-th-list nav-icon text-gradient-primary"></i>
              <p>Vote</p>
            </a>
          </li>  
        </ul>
      </nav>
    </div>
  </aside>
  




  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper  bg-dark">
  	 <div class="toast" id="alert_toast" role="alert" aria-live="assertive" aria-atomic="true">
	    <div class="toast-body text-white">
	    </div>
	  </div>
    <div id="toastsContainerTopRight" class="toasts-top-right fixed"></div>
    <!-- Content Header (Page header) -->
    <div class="content-header">
     
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid text-dark viewer-panel" style="margin-bottom: 4rem">
        <div class="container-fluid">
          <div class="row mb-2">
          </div><!-- /.row -->
            <hr class="border-primary">
        </div><!-- /.container-fluid -->
        <div class="header">
              <table cellpadding="0" cellspacing="0" height="230" id="table">
                <tr style="color:#7E7E7E; ">
                  <th style="border-bottom:1px dashed #CCC">Title</th>
                  <th style="border-bottom:1px dashed #CCC">Statistics</th>
                </tr>
                <?php 
        $connection = mysqli_connect("localhost","root","") ;

        $db_select = mysqli_select_db($connection,"music_db"); 
        if (!$db_select) {
        
            error_log("Database selection failed: " . mysqli_error($connection));
        
            die('Internal server error');
        
        }
				$getVotes = mysqli_query($connection,"SELECT songfile,songpoints FROM tblsongs WHERE songpoints >=1 ORDER BY songpoints DESC LIMIT 6");
				while($row = mysqli_fetch_array($getVotes)){
				$r = rand(128,255);
				$g = rand(128,255);
				$b = rand(128,255);
				$color = dechex($r).dechex($g).dechex($b);
        		?>
                <tr>
                  <td align="left" width="228" style="border-bottom:1px dashed #CCC; text-indent:5px;">
                    <?php echo preg_replace("/\\.[^.\\s]{3,4}$/", "", $row['songfile'])?></td>
                  <td style="border-bottom:1px dashed #CCC; text-indent:5px;" width="142">
                    <div
                      style="background:#<?php echo $color?>;width:<?php echo $row['songpoints']?>px; height:22px; font-size:11px; height:15px;">
                    </div>
                  </td>
                </tr>
                <?php
                }
                ?>
              </table>
            </div>
          </div>
      </div><!--/. container-fluid -->
      
      <style>
            audio::-webkit-media-controls {
                width: inherit;
                height: inherit;
                position: relative;
                direction: ltr;
                display: flex;
                flex-direction: column;
                justify-content: flex-end;
                align-items: center;
                background: white;
            }
            audio::-webkit-media-controls-enclosure, video::-webkit-media-controls-enclosure {
                width: 100%;
                max-width: 800px;
                height: 30px;
                background: none;
                flex-shrink: 0;
                bottom: 0;
                text-indent: 0;
                padding: 0;
                box-sizing: border-box;
            }
            audio::-webkit-media-controls-play-button{
                display: none;
               
            }
            .audio-control-btn:hover{
              color:white
            }
               
          </style>
          
      <div id="audio-player" class="d-flex w-100 justify-content-end align-items-center bg-dark py-1 position-absoulute">
        
            <button class="btn prev-player audio-control-btn" onclick="_prev($(this))" data-type="play"><i class="fa fa-step-backward"></i></button>
            <button class="btn p-player audio-control-btn" onclick="_player($(this))" data-queue="0" data-type="play" style="font-size: 25px"><i class="fa fa-play"></i></button>
            <button class="btn next-player audio-control-btn" onclick="_next(-1,1)" data-type="play"><i class="fa fa-step-forward"></i></button>
            <audio controls class="bg-black" ended="nextAudioNode.play();" id="mplayer">
              
            </audio>
        </div>
    </section>
    <script>
      function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) == ' ') {
            c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
          }
        }
        return "";
      }
      var src_arr = {};
      $(document).ready(function(){
        if(getCookie('src') != ''){
          var parsed = JSON.parse(getCookie('src'))
          var q = getCookie('pq') != '' ? getCookie('pq') : '';
          play_music(parsed,q,0)
        }
      })
      function _player(_this){
        var type = _this.attr('data-type')
        if($('#mplayer source').length <= 0)
          return false;
        if(type == 'play'){
          _this.attr('data-type','pause')
          _this.html('<i class="fa fa-pause"></i>')
          document.getElementById('mplayer').play()
        }else{
          _this.attr('data-type','play')
           _this.html('<i class="fa fa-play"></i>')
          document.getElementById('mplayer').pause()
        }
      }

      function play_music($src,$i=0,$p = 1){
      var audio = $('<audio controls class="bg-black" id="mplayer" data-queue = "'+$i+'"></audio>')
          if(typeof $src === 'object'){
             src_arr = $src;
           $src = $src[$i].upath
         }

         document.cookie = "src="+JSON.stringify(src_arr)
         document.cookie = "pq="+$i
          var csrc = $('#mplayer source').attr("src")
          if( $src != csrc){
             audio.append('<source src="'+$src+'">')
             var player=  $('#audio-player')
             player.find('audio').remove()
             player.append(audio)
             get_details(src_arr[$i].id)
           }else{
             if(!document.getElementById('mplayer').paused == true){
               document.getElementById('mplayer').pause()
               return false;
             }
           }
        
       
         // curA.remove()
         if($p == 1){
          document.getElementById('mplayer').play()
           $('.p-player').attr('data-type','pause')
          $('.p-player').html('<i class="fa fa-pause"></i>')
         }
          m_end()
      }
      function _prev($i=-1){
          $i=parseInt($('#mplayer').attr('data-queue'))-1;
        if(!!src_arr[$i]){
           var audio = $('<audio controls class="bg-black" id="mplayer" data-queue = "'+$i+'"></audio>')
         document.cookie = "pq="+$i

        audio.append('<source src="'+src_arr[$i].upath+'">')
             var player=  $('#audio-player')
             player.find('audio').remove()
             player.append(audio)
             get_details(src_arr[$i].id)
            document.getElementById('mplayer').play()
           $('.p-player').attr('data-type','pause')
          $('.p-player').html('<i class="fa fa-pause"></i>')
           m_end()
        }
      }
      function _next($i=-1,$p=0){
        if($i == -1)
        { 
          $i=parseInt($('#mplayer').attr('data-queue'))+1
        }
        if(!!src_arr[$i] && !!src_arr[$i].upath){
         document.cookie = "pq="+$i
           var audio = $('<audio controls class="bg-black" id="mplayer" data-queue = "'+$i+'"></audio>')
        audio.append('<source src="'+src_arr[$i].upath+'">')
             var player=  $('#audio-player')
             player.find('audio').remove()
             player.append(audio)
             get_details(src_arr[$i].id)
            document.getElementById('mplayer').play()
           $('.p-player').attr('data-type','pause')
          $('.p-player').html('<i class="fa fa-pause"></i>')
           m_end()
        }else{
           play_music(src_arr,0,$p)
        }
      }
      function m_end(){
        document.getElementById('mplayer').addEventListener('ended',function(){
           $('.p-player').attr('data-type','play')
            $('.p-player').html('<i class="fa fa-play"></i>')
            document.getElementById('mplayer').duration = 0
            _next(parseInt($('#mplayer').attr('data-queue'))+1)
        })
      }
      window.addEventListener('popstate', function(e){
         var nl = new URLSearchParams(window.location.search);
            var page =nl.get('page')
        $.ajax({
          url:"controller.php"+window.location.search,
          success:function(resp){
           $('.viewer-panel').html(resp)
          }
        })
      });
      function get_details($id){
        $.ajax({
          url:"ajax.php?action=get_details",
          method:'POST',
          data:{id:$id},
          success:function(resp){
            if(resp){
              resp = JSON.parse(resp)
              var _html = '<div id="pdet" class="d-flex justify-content-center align-items-center"><img src="assets/uploads/'+resp.cover_image+'" alt="" class="img-thumbnail bg-gradient-1" style="width: 50px;height: 50px;object-fit: cover"><div class="ml-2 mr-4"><div><b><large>'+resp.title+'</large></b></div><div><b><small>'+resp.artist+'</small></b></div></div></div>'
              if($('#audio-player #pdet').length > 0)
                $('#audio-player #pdet').remove()
              $('#audio-player').prepend(_html)
            }
          }
        })
      }
      _anchor()
      function _anchor(){
      $('a').click(function(e){
        e.preventDefault()
        var _h=  $(this).attr("href");
        if(document.href == _h){
          return false
        }
        window.history.pushState({}, null, $(this).attr("href"));
        var page = '<?php echo isset($_GET['page']) ? $_GET['page'] : 'home' ?>';
          var nl = new URLSearchParams(window.location.search);
          var page =nl.get('page')
          $('.nav-link').removeClass('active')
          if($('.nav-link.nav-'+page).length > 0){
            $('.nav-link.nav-'+page).addClass('active')
            if($('.nav-link.nav-'+page).hasClass('tree-item') == true){
              $('.nav-link.nav-'+page).closest('.nav-treeview').siblings('a').addClass('active')
              $('.nav-link.nav-'+page).closest('.nav-treeview').parent().addClass('menu-open')
            }
            if($('.nav-link.nav-'+page).hasClass('nav-is-tree') == true){
              $('.nav-link.nav-'+page).parent().addClass('menu-open')
            }

          }
        $.ajax({
          url:"controller.php"+window.location.search,
          success:function(resp){
           $('.viewer-panel').html(resp)
           _anchor()
          }
        })
      })
      }
      function _redirect($url){
          window.history.pushState({}, null, $url);
          var page = '<?php echo isset($_GET['page']) ? $_GET['page'] : 'home' ?>';
            var nl = new URLSearchParams(window.location.search);
            var page =nl.get('page')
            $('.nav-link').removeClass('active')
            if($('.nav-link.nav-'+page).length > 0){
              $('.nav-link.nav-'+page).addClass('active')
              if($('.nav-link.nav-'+page).hasClass('tree-item') == true){
                $('.nav-link.nav-'+page).closest('.nav-treeview').siblings('a').addClass('active')
                $('.nav-link.nav-'+page).closest('.nav-treeview').parent().addClass('menu-open')
              }
              if($('.nav-link.nav-'+page).hasClass('nav-is-tree') == true){
                $('.nav-link.nav-'+page).parent().addClass('menu-open')
              }

            }
          $.ajax({
            url:"controller.php"+window.location.search,
            success:function(resp){
             $('.viewer-panel').html(resp)
             _anchor()
            }
          })
      }
    </script>
    <!-- /.content -->
    <div class="text-dark">
    <div class="modal fade" id="confirm_modal" role='dialog'>
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title">Confirmation</h5>
      </div>
      <div class="modal-body">
        <div id="delete_content"></div>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-primary" id='confirm' onclick="">Continue</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div> -->
      </div>
    </div>
  </div>
  <div class="modal fade" id="uni_modal" role='dialog'>
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title"></h5>
      </div>
      <div class="modal-body">
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-primary" id='submit' onclick="$('#uni_modal form').submit()">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div> -->
      </div>
    </div>
  </div>
  <div class="modal fade" id="uni_modal_right" role='dialog'>
    <div class="modal-dialog modal-full-height  modal-md" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span class="fa fa-arrow-right"></span>
        </button>
      </div>
      <div class="modal-body">
      </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="viewer_modal" role='dialog'>
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
              <button type="button" class="btn-close" data-dismiss="modal"><span class="fa fa-times"></span></button>
              <img src="" alt="">
      </div>
    </div>
  </div>
  </div>
  <!-- /.content-wrapper -->
</div>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer bg-black">
    <strong>Copyright &copy; 2020 <a href="http://naldiprofile.great-site.net">Donaldi</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Simple Music Clound Community.</b>
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<!-- Bootstrap -->
<?php include 'footer.php' ?>
</body>
</html>

</body>
</html>